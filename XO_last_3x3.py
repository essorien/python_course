import pygame

# Создание массива
mas = [[0] * 3 for i in range(3)]

# Размер игрового поля и блоков
size_block = 100
margin = 15
width = height = size_block * 3 + margin * 4
size_window = (width, height)

# Название окна и иконка
screen = pygame.display.set_mode(size_window)
pygame.display.set_caption("Крестики-нолики")
#img = pygame.image.load('ikonXO.jpg')
#pygame.display.set_icon(img)

# Установка цветов
black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 0)
white = (255, 255, 255)

# Создание массива
mas = [[0] * 3 for i in range(3)]

# переменные
query = 0  # 1 2 3 4 5 6 7 8 9
zeroes = 0


# функция поиска победителя
def check_win(mas, sign):
    for row in mas:
        zeroes = 0
        zeroes += row.count(0)
        if row.count(sign) == 3:
            return sign
    for col in range(3):
        if mas[0][col] == sign and mas[1][col] == sign and mas[2][col] == sign:
            return sign
    if mas[0][0] == sign and mas[1][1] == sign and mas[2][2] == sign:
        return sign
    if mas[0][2] == sign and mas[1][1] == sign and mas[2][0] == sign:
        return sign
    if zeroes == 0:
        return 'Ничья'
    return False


# Начало работы , определение условий закрытия
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit()
        # Получение координат клика
        elif event.type == pygame.MOUSEBUTTONDOWN:
            x_mouse, y_mouse = pygame.mouse.get_pos()
            col = x_mouse // (size_block + margin)
            row = y_mouse // (size_block + margin)
            if mas[row][col] == 0:
                if query % 2 == 0:
                    mas[row][col] = 'x'
                else:
                    mas[row][col] = 'o'
                query += 1
    # Отрисовка поля и квадратиков
    for row in range(3):
        for col in range(3):
            if mas[row][col] == 'x':
                color = red
            elif mas[row][col] == 'o':
                color = green
            else:
                color = white
            x = col * size_block + (col + 1) * margin
            y = row * size_block + (row + 1) * margin
            pygame.draw.rect(screen, color, (x, y, size_block, size_block))
            # Отрисовка крестиков и нулей
            if color == red:
                pygame.draw.line(screen, white, (x + 5, y + 5), (x + size_block - 5, y + size_block - 5), 3)
                pygame.draw.line(screen, white, (x + size_block - 5, y + 5), (x + 5, y + size_block - 5), 3)
            elif color == green:
                pygame.draw.circle(screen, white, (x + size_block // 2, y + size_block // 2), size_block // 2 - 4, 3)
    if (query - 1) % 2 == 0:  # крестики
        game_over = check_win(mas, 'x')
    else:
        game_over = check_win(mas, 'o')

    if game_over:
        print (game_over)

    pygame.display.update()
