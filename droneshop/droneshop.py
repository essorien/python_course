from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.image import Image


class MainPage(GridLayout):

    def __init__(self, **kwargs):
        super(MainPage, self).__init__(**kwargs)
        self.cols = 2
        n1 = GridLayout(cols=2)
        n1.add_widget(Image(source="mainLogo.png"))
        n1.add_widget(Label(text="[color=ff3333]First Drone Shop[/color]", font_size = '30sp', markup = True))
        n2 = GridLayout(cols=1)
        n2.add_widget(Image(source="Mavic Air.png"))
        n22 = GridLayout(cols=2)
        n22.add_widget(Label(text="DJI Mavic Air"))
        n22.add_widget(Label(text="Стоимость: 55.000 рублей"))
        n2.add_widget(n22)
        n2.add_widget(Button(text="Подробнее"))
        n3 = GridLayout(cols=1)
        n3.add_widget(Image(source="MavicMini.png"))
        n33 = GridLayout(cols=2)
        n33.add_widget(Label(text="DJI Mavic Mini"))
        n33.add_widget(Label(text="Стоимость: 30.000 рублей"))
        n3.add_widget(n33)
        n3.add_widget(Button(text="Подробнее"))
        n4 = GridLayout(cols=1)
        n4.add_widget(Image(source="Phantom 4.png"))
        n44 = GridLayout(cols=2)
        n44.add_widget(Label(text="DJI Phantom 4"))
        n44.add_widget(Label(text="Стоимость: 80.000 рублей"))
        n4.add_widget(n44)
        n4.add_widget(Button(text="Подробнее"))
        n5 = GridLayout(cols=1)
        n5.add_widget(Image(source="Inspire.png"))
        n55 = GridLayout(cols=2)
        n55.add_widget(Label(text="DJI Inspire"))
        n55.add_widget(Label(text="Стоимость: 280.000 рублей"))
        n5.add_widget(n55)
        n5.add_widget(Button(text="Подробнее"))







        n1.add_widget(n2)
        n1.add_widget(n3)
        n1.add_widget(n4)
        n1.add_widget(n5)
        self.add_widget(n1)


class MyApp(App):
    def build(self):
        return MainPage()


if __name__ == '__main__':
    MyApp().run()
