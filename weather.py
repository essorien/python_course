from waitress import serve


def weather():
    from pyowm import OWM
    from pyowm.utils.config import get_default_config
    config_dict = get_default_config()
    config_dict['language'] = 'ru'
    owm = OWM('b5619b3b7035cbb82e14a50abb70679f')
    # place = input('Введите название города: ')
    place = 'London,GB'

    mgr = owm.weather_manager()
    observation = mgr.weather_at_place(place)
    pogoda = observation.weather
    w = pogoda.detailed_status
    temp = (pogoda.temperature('celsius'))['temp']
    max_temp = (pogoda.temperature('celsius'))['temp_max']
    wind = (pogoda.wind())['speed']
    # print('В городе ' + place + " сейчас " + w + ', ' + str(temp) + ' С°')
    # print('Максимальная температура составит: ' + str(max_temp) + ' С°')
    # print('Скорость ветра составит ' + str(wind) + ' м/с')
    return 'В городе ' + place + " сейчас " + w + ', ' + str(temp) + ' С°.' \
           + ' Максимальная температура воздуха на сегодня: ' + str(max_temp) + ' С°.' + \
           ' Скорость ветра составит ' + str(wind) + ' м/с.'

def app(environ, start_response):
    headers = [('Content-Type', 'text/html;charset=utf-8')]
    start_response('200 OK', headers)
    return [weather().encode('utf8')]


if __name__ == '__main__':
    serve(app, host='0.0.0.0', port='8080')
