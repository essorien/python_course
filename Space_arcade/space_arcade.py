import turtle
from random import random
from random import uniform
from turtle import Vec2D

win = turtle.Screen()
win.title('Turtle Race')
win.setup(700, 700)
win.bgpic('NASA.png')

forward_vec = Vec2D(0, -10)
back_vec = Vec2D(0, 10)
right_vec = Vec2D(20, 0)
left_vec = Vec2D(-20, 0)
t = turtle.Turtle()
t.screen.addshape('asteroid.gif')
t.screen.addshape('giphy.gif')
t.shape('giphy.gif')
# t.seth(90)
t.up()
t.goto(0, -250)


def generate_random():
    return random(), random(), random(),


def move_turtle():
    turtles = win.turtles()

    for t2 in turtles:
        if t2 == t:
            continue

        pos = t2.pos()
        d = Vec2D(pos[0], pos[1])
        move = d + forward_vec
        t2.goto(move)

    turtle.ontimer(move_turtle, 70)


def random_color():
    turtles = win.turtles()
    for t2 in turtles:
        if t2 == t:
            continue
    t2.color(generate_random(), generate_random())


def timer_color():
    random_color()
    turtle.update()
    turtle.ontimer(timer_color, 100)


def add_turtle():
    turtle.tracer(False)
    t1 = turtle.Turtle()
    t1.penup()
    t1.shape('asteroid.gif')
    x1 = uniform(-250, 250)
    y1 = 300
    t1.goto(x1, y1)
    random_color()
    turtle.update()


def add_turtle2():
    add_turtle()
    turtle.ontimer(add_turtle2, 1000)


def go_right():
    pos = t.pos()
    d = Vec2D(pos[0], pos[1])
    move = d + right_vec
    t.goto(move)


def go_left():
    pos = t.pos()
    d = Vec2D(pos[0], pos[1])
    move = d + left_vec
    t.goto(move)


def go_forward():
    pos = t.pos()
    d = Vec2D(pos[0], pos[1])
    move = d + forward_vec
    t.goto(move)


def go_back():
    pos = t.pos()
    d = Vec2D(pos[0], pos[1])
    move = d + back_vec
    t.goto(move)


# попытка удалить падающих черепах
def hide_turtle():
    turtles = win.turtles()
    for t2 in turtles:
        if t2 == t:
            continue

    if t2.ycor() > -400:
        t2.isvisible()


#turtle.onkeyrelease(add_turtle, key='p')
turtle.ontimer(timer_color, 1)
turtle.onkeyrelease(go_right, key='d')
turtle.onkeyrelease(go_left, key='a')
turtle.onkeyrelease(go_forward, key='s')
turtle.onkeyrelease(go_back, key='w')
turtle.listen()

add_turtle2()
move_turtle()

turtle.done()
